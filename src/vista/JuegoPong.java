package vista;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import modelo.Pelota;
import modelo.Raqueta;

public class JuegoPong extends JPanel{
	
	private Font ePuntaje;
	private Font eNivel;
	private static Raqueta raqueta;
	private ArrayList<Pelota> pelotas;
	private int puntajeJugador;
	private int nivel;
	private boolean existePelotaNueva;
	private ImageIcon[] imagenes;
	private final static int ANCHO=400;
	private final static int ALTO=600;
	public JuegoPong() {
		super();
		raqueta=new Raqueta(175,550,50,10);
		pelotas=new ArrayList<Pelota>();
		
		pelotas.add(new Pelota((int)(Math.random()*ANCHO), 10, 15,1));
		//pelotas.add(new Pelota(40, 10, 15,1));
		nivel=1;
		existePelotaNueva=false;
		imagenes=new ImageIcon[10];
		imagenes[0] = new ImageIcon(getClass().getResource("/images/1.jpeg")); 
		imagenes[1] = new ImageIcon(getClass().getResource("/images/2.jpeg")); 
		imagenes[2] = new ImageIcon(getClass().getResource("/images/3.jpg")); 
		imagenes[3] = new ImageIcon(getClass().getResource("/images/4.jpg")); 
		imagenes[4] = new ImageIcon(getClass().getResource("/images/5.jpg")); 
		imagenes[5] = new ImageIcon(getClass().getResource("/images/6.jpeg")); 
		imagenes[6] = new ImageIcon(getClass().getResource("/images/7.jpg")); 
		imagenes[7] = new ImageIcon(getClass().getResource("/images/8.jpg")); 
		imagenes[8] = new ImageIcon(getClass().getResource("/images/9.jpeg")); 
		imagenes[9] = new ImageIcon(getClass().getResource("/images/10.jpeg")); 

	}
	@Override
	public void paint(Graphics g) {
		super.paint(g);
	
		ePuntaje= new Font("Monospaced",Font.BOLD,18);
		eNivel= new Font("Monospaced",Font.BOLD,18);
		
		g.setFont(ePuntaje);
		g.setFont(eNivel);
		
		Dimension height = getSize();
		 
		g.drawImage(imagenes[nivel-1].getImage(), 0, 0, height.width, height.height, null);
		 
		g.setColor(Color.WHITE);
		g.drawString("Puntaje: "+puntajeJugador, 270,20);
		g.drawString("Nivel: "+nivel, 270,40);
		raqueta.dibujar(g);
		for(Pelota p: pelotas) {
			p.dibujar(g);
		}
	}
	public static void main(String[]args) throws InterruptedException {
		JFrame ventana=new JFrame("Juego de ping pong");
		boolean seguirJugando=true;
		ventana.setSize(ANCHO, ALTO);
		ventana.setResizable(false);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JuegoPong juegoPong= new JuegoPong();
		
		ventana.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {}
			
			@Override
			public void keyReleased(KeyEvent arg0) {}
			
			@Override
			public void keyPressed(KeyEvent e) {
				String tecla=e.getKeyText(e.getKeyCode());
				if(tecla.equals("Izquierda")){
					raqueta.goLeft();
				}
				else if(tecla.equals("Derecha")) {
					raqueta.goRight();
				}
				
			}
		});
		
		ventana.add(juegoPong);
		 
		ventana.setVisible(true);
		
		while(seguirJugando) {
			juegoPong.repaint();
			
			if(juegoPong.pelotaRebasaRaqueta()) {
				JOptionPane.showMessageDialog(null, "Se ha terminado el juego", "Ping Pong", JOptionPane.WARNING_MESSAGE);
				seguirJugando=preguntarReinicio(juegoPong);
			}
			juegoPong.moverPelota();
			juegoPong.hayColisionRaquetaPelota();
			juegoPong.subirDeNivel();
			if(juegoPong.puntajeJugador==50/5) {
				JOptionPane.showMessageDialog(null, "Ganaste!", "Ping Pong", JOptionPane.WARNING_MESSAGE);
				seguirJugando=preguntarReinicio(juegoPong);
			}
			Thread.sleep(10);
		}
		ventana.dispose();
	}
	public static boolean preguntarReinicio(JuegoPong juegoPong) {
		boolean bandera=true;
		if(JOptionPane.showConfirmDialog(null, "¿Quieres volver a jugar?","Ping Pong", JOptionPane.YES_NO_OPTION)==0){
			juegoPong.reiniciarJuego();
		}
		else {
			bandera=false;
		}
		return bandera;
	}
	public void subirDeNivel() {
		switch(puntajeJugador) {
			case 45/5:
				getPelotas().get(0).setVelocidad(1);
				nivel=2;
				break;
			case 10/5:
				getPelotas().get(0).setVelocidad(2);
				nivel=3;
				break;
			case 15/5:
				getPelotas().get(0).setVelocidad(2);
				nivel=4;
				break;
			case 20/5:
				getPelotas().get(0).setVelocidad(3);
				nivel=5;
				break;
			case 25/5:
				getPelotas().get(0).setVelocidad(3);
				nivel=6;
				break;
			case 30/5:
				getPelotas().get(0).setVelocidad(3);
				nivel=7;
				break;
			case 35/5:
				getPelotas().get(0).setVelocidad(3);
				nivel=8;
				break;
			case 40/5:
				getPelotas().get(0).setVelocidad(4);
				nivel=9;
				break;
			case 5/5:
				getPelotas().get(0).setVelocidad(2);
				if(!existePelotaNueva) {
					pelotas.add(new Pelota(40, 10, 15,1));
					getPelotas().get(1).setVelocidad(2);
					nivel=10;
					existePelotaNueva=true;
				}
				break;
		}
	}
	public boolean pelotaRebasaRaqueta() {
		boolean bandera=false;
		for(Pelota p: pelotas) {
			if(p.getY()>=raqueta.getY()) {
				bandera=true;
				break;
			}
		}
		return bandera;
	}
	public void hayColisionRaquetaPelota() {
		for(Pelota pelota: pelotas) {
			if(pelota.getY()+pelota.getDiametro() >= raqueta.getY() &&
					pelota.getY()+pelota.getDiametro()<=raqueta.getY()+raqueta.getAlto()) {
				System.out.println("pelota:  "+pelota.getX()+":"+pelota.getY()+
						" raqueta: "+raqueta.getX()+":"+raqueta.getY());
				//Verifica si la pelota choca en el lado izquierdo de la raqueta
				if(pelota.getX()>=raqueta.getX() && pelota.getX()<raqueta.getX()+raqueta.getAncho()/2) {
					System.out.println("Pego en el lado izquierdo");
					pelota.direccionDerecha=false;
					pelota.direccionAbajo=false;
					pelota.disminuirX();
					puntajeJugador++;
				}
				//Verifica si la pelota choca en el lado derecho de la raqueta si es gusto por
				//la mitad se considera que fue del lado derecho
				if(pelota.getX()>=raqueta.getX()+raqueta.getAncho()/2 && 
						pelota.getX()<=raqueta.getX()+raqueta.getAncho()){
					System.out.println("Si pengo en el lado derecho");
					pelota.direccionDerecha=true;
					pelota.direccionAbajo=false;
					pelota.disminuirY();
					puntajeJugador++;
				}
			}
		}
	}
	public void moverPelota() {
		
		for(Pelota pelota: pelotas) {
			if(pelota.direccionDerecha) {
				pelota.incrementarX();
			}
			else {
				pelota.disminuirX();
			}
			if(pelota.direccionAbajo) {
				pelota.incrementarY();
			}
			else {
				pelota.disminuirY();
			}
			
			if(pelota.getY()>=getHeight()-pelota.getDiametro()) {
				
				pelota.disminuirY();
				pelota.direccionAbajo=false;
			}
			
			if(pelota.getY() <= pelota.getDiametro()) {
				pelota.incrementarY();
				pelota.direccionAbajo=true;
			}
			
			if(pelota.getX() >= getWidth()-pelota.getDiametro()) {
				pelota.disminuirX();
				pelota.direccionDerecha=false;
			}
			if (pelota.getX()<=0) {
				pelota.incrementarX();
				pelota.direccionDerecha=true;
			}
		}
	}
	
	public void reiniciarJuego() {
		nivel=1;
		puntajeJugador=0;
		pelotas=new ArrayList<Pelota>();
		pelotas.add(new Pelota(10, 10, 15,1));
		existePelotaNueva=false;
	}
	public ArrayList<Pelota> getPelotas() {
		return pelotas;
	}
	public void setPelotas(ArrayList<Pelota> pelotas) {
		this.pelotas = pelotas;
	}
	
	
}
