package modelo;

import java.awt.Graphics;

public class Raqueta {
	private int ancho;
	private int alto;
	private int x;
	private int y;
	
	public Raqueta(int x,int y,int ancho,int alto) {
		
		this.x=x;
		this.y=y;
		this.ancho=ancho;
		this.alto=alto;
	}
	
	public void dibujar(Graphics g) {
		g.fillRect(x, y, ancho, alto);
	}
	public void goLeft() {
		if(x>=15) {
			this.x=this.x-15;
		}
	}
	public void goRight() {
		if(x<385-ancho) {
			this.x=this.x+15;
		}
	}
	public int getAncho() {
		return ancho;
	}
	public void setAncho(int ancho) {
		this.ancho = ancho;
	}
	public int getAlto() {
		return alto;
	}
	public void setAlto(int alto) {
		this.alto = alto;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
}
