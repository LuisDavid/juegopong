package modelo;

import java.awt.Graphics;

public class Pelota {
	private int diametro;
	private int x;
	private int y;
	private int velocidad;
	public boolean direccionAbajo;
	public boolean direccionDerecha;
	public Pelota(int x,int y,int diametro,int velocidad) {
		this.x=x;
		this.y=y;
		this.diametro=diametro;
		direccionAbajo=true;
		direccionDerecha=Math.random()<0.5;
		this.velocidad=velocidad;
	}
	public void dibujar(Graphics g) {
		g.fillOval(x, y, diametro, diametro);
	}
	public int getDiametro() {
		return diametro;
	}

	public void setDiametro(int perimetro) {
		this.diametro = perimetro;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public void incrementarX() {
		this.x=this.x+velocidad;
	}
	public void incrementarY() {
		this.y=this.y+velocidad;
	}
	public void disminuirX() {
		this.x=this.x-velocidad;
	}
	public void disminuirY() {
		this.y=this.y-velocidad;
	}
	public int getVelocidad() {
		return velocidad;
	}
	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	
}
